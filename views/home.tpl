<!DOCTYPE html>
<html>
  <head>
    <title>eZaliv</title>
    <meta charset="utf-8">
  </head>
  <body>
    <header>
      <h1>eZaliv</h1>
    </header>

    <section id="vnos">
      <h2>Kategorije izdelkov</h2>
      <ul>
% for id, kat in kategorije.items():
        <li><a href="./kategorija/{{id}}">{{kat}}</a></li>
% end
      </ul>
    </section>
    
    <hr />

    <section id="vnos">
      <p> <a href="./admin/">Vstop za administratorja</a> </p>
    </section>

    
    <footer>  
    </footer>

  </body>
</html>
